
import numpy as np
import numpy.ma as ma
from scipy.interpolate import griddata


def reinterp(logT, logNe, data, refinement=1):
    # Define new uniform grid points
    x = np.linspace(logT.min(), logT.max(), refinement * data.shape[0])
    y = np.linspace(logNe.min(), logNe.max(), refinement * data.shape[1])
    xx, yy = np.meshgrid(x, y, indexing="ij")

    if not isinstance(data, ma.MaskedArray):
        data = ma.masked_invalid(data)

    xm = xx[~data.mask]
    ym = yy[~data.mask]
    values = data[~data.mask]

    z = griddata((xm, ym), values.ravel(), (xx, yy), method='cubic')

    return (x, y, z)


def reinterp_multispecies(logT, logNe, data, refinement=1):
    nspec = data.shape[2]
    z = np.zeros((data.shape[0], data.shape[1], nspec))
    for s in range(nspec):
        x, y, z[:, :, s] = reinterp(logT, logNe, data[:, :, s], refinement=refinement)
    return x, y, z


def cross_validation_score(logT, logNe, data, refinement=1):
    """
    Performs leave-one-out cross validation to get the normalized root-mean-square-error estimate
    for the re-interpolation of the data. We only remove interior elements of the data grid to avoid
    extrapolation.
    """

    rmseArr = np.zeros(data.shape[2])
    for s in range(data.shape[2]):
        rmse = 0.0
        count = 0

        for i in range(1,data.shape[0]-1):
            for j in range(1,data.shape[1]-1):
                actual = data[i,j,s]

                # Exclude the current point from data
                data[i, j, s] = np.nan
                maskedData = ma.masked_invalid(data[:, :, s])

                # Re-interpolate with the masked data
                _, _, z = reinterp(logT, logNe, maskedData, refinement=refinement)

                # Measure how close interpolated point is to the actual point
                rmse += (z[i, j] - actual) ** 2
                count += 1

                # Replace nan value
                data[i, j, s] = actual

        rmseArr[s] = np.sqrt(rmse / count) / np.mean(data[:, :, s])

    return rmseArr.max()
