#!/bin/bash

module purge
source /opt/cray/pe/cpe/23.12/restore_lmod_system_defaults.sh
module load cpe/23.12 \
  craype-x86-milan \
  libfabric/1.15.2.0 \
  craype-network-ofi \
  xpmem/2.6.2-2.5_2.38__gd067c3f.shasta \
  perftools-base/23.12.0 \
  cudatoolkit/12.2 \
  craype-accel-nvidia80 \
  gpu/1.0 \
  nvhpc/23.9 \
  craype/2.7.30 \
  cray-dsmml/0.2.2 \
  cray-mpich/8.1.28 \
  cray-libsci/23.12.5 \
  PrgEnv-nvhpc/8.5.0 \
  cmake/3.24.3
