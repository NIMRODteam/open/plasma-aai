"""
This script uses Aurora to get ADAS data and writes it to an HDF5 file.
Install Aurora with: pip install aurorafusion
Call with atomic symbol, e.g., for carbon: python make_adata_file.py C
https://aurora-fusion.readthedocs.io/en/latest/index.html
"""

import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt
from adata.reinterp import reinterp_multispecies, cross_validation_score

filelist = ["acd", # effective recombination 
            "ccd", # effective charge exchange recombination 
            "plt", # effective low-level line power
            "prb", # effective recombination + Brems power
            "prc", # effective charge exchange power
            "scd"  # effective ionization
            ]

def plot_points(logT, logNe):
    X, Y = np.meshgrid(logT, logNe)
    plt.plot(X, Y, marker="o", color="k", linestyle="none")
    plt.xlabel("log(T) eV")
    plt.ylabel("log(Ne) m^{-3}")
    plt.show()


def plot_countour(X, Y, Z):
    plt.contourf(X, Y, Z)
    plt.xlabel("log(T) eV")
    plt.ylabel("log(Ne) m^{-3}")
    plt.show()


def read_hdf5_file(file_path, key):
    logT = None
    logNe = None
    with h5py.File(file_path, "r") as f:
        dataset = f[key]
        data = np.array(dataset["logdata"])
        logT = np.array(dataset["logT"])
        logNe = np.array(dataset["logNe"])

    return logT, logNe, data


def rewrite(file_name, reinterpolate=False, refinement=1):
    for fname in filelist:
        logT, logNe, data = read_hdf5_file(file_name, fname)
        if reinterpolate:
            rmse = cross_validation_score(logT, logNe, data, refinement)
            print(f'{fname} Maximum Re-interpolation nRMSE = {rmse:.6e}')
            new_logT, new_logNe, new_data = reinterp_multispecies(logT, logNe, data, refinement=refinement)
        else:
            new_logT, new_logNe, new_data = logT, logNe, data
        with h5py.File(f'{file_name}', "r+") as f:
            dataset = f[fname]
            dataset["logdata"][...] = new_data
            dataset["logT"][...] = new_logT
            dataset["logNe"][...] = new_logNe


def write_test_file(nspec, file_name, size=(10, 10)):
    # Create an HDF5 file
    with h5py.File(file_name, "w") as hdf:
        # Define the groups
        groups = ["acd", "ccd", "plt", "prb", "prc", "scd"]

        for group in groups:
            # Create the group
            grp = hdf.create_group(group)

            # Create logT with equally spaced entries from -1 to 5
            logT = np.linspace(-1, 5, size[0])
            grp.create_dataset("logT", data=logT)

            # Create logNe with equally spaced entries from 5.0 to 20.0
            logNe = np.linspace(5.0, 20.0, size[1])
            grp.create_dataset("logNe", data=logNe)

            # Generate logdata from a cubic function with random x-values
            data = np.zeros((size[0], size[1], nspec))
            for i in range(nspec):
                x_values = np.random.uniform(-2, -0.25, (len(logT), len(logNe)))
                data[:, :, i] = x_values**3 - 5 * x_values**2 + 2 * x_values + 1
                # Ensure monotonicity 
                data[:, :, i].sort(axis=1)
                data[:, :, i].sort(axis=0)

            grp.create_dataset("logdata", data=data)


def fetch_and_write(symbol, file_name, reinterpolate=False, refinement=1):
    import aurora
    adict = aurora.atomic.get_atom_data(symbol, files=filelist)
    with h5py.File(file_name, "w") as f:
        for fname in filelist:
            if isinstance(adict[fname], tuple):  # Check if data type is a tuple
                data = np.moveaxis(adict[fname][2], 0, 2)
                logT = adict[fname][1]
                logNe = adict[fname][0]
  
            else:
                data = np.moveaxis(adict[fname].logdata, 0, 2)
                logT = adict[fname].logT
                logNe = adict[fname].logNe
            
            if reinterpolate:
                rmse = cross_validation_score(logT, logNe, data, refinement)
                print(f'{fname} Maximum Re-interpolation nRMSE = {rmse:.6e}')
                logT, logNe, data = reinterp_multispecies(logT, logNe, data, refinement=refinement)

            grp = f.create_group(fname)
            grp.create_dataset(
                "logdata", data=data
            )
            grp.create_dataset("logT", data=logT)
            grp.create_dataset("logNe", data=logNe)


def main():
    # Set up argument parsing
    parser = argparse.ArgumentParser(description="Process HDF5 files.")
    parser.add_argument(
        "mode",
        choices=["fetch_and_write", "write_test_file", "rewrite"],
        help="Choose the mode of operation.",
    )
    parser.add_argument(
        "--symbol",
        help="Atomic symbol for fetching data (required for fetch_and_write mode).",
    )
    parser.add_argument(
        "--nspec",
        default=2,
        type=int,
        help="Number of species for generating synthetic data (required for write_test_file mode).",
    )
    parser.add_argument(
        "--file_name", default="output.h5", help="Name of the HDF5 file to create."
    )
    parser.add_argument(
        "--reinterp", action="store_true", default=False, help="Re-interpolate the data onto a uniform grid."
    )

    args = parser.parse_args()

    if args.mode == "write_test_file":
        write_test_file(args.nspec, args.file_name)
        print(f"Test file '{args.file_name}' created.")
    elif args.mode == "fetch_and_write":
        if not args.symbol:
            parser.error("The 'fetch_and_write' mode requires an atomic symbol.")
        fetch_and_write(args.symbol, args.file_name, args.reinterp)
        print(f"Data for symbol '{args.symbol}' written to '{args.file_name}'.")
    elif args.mode == "rewrite":
        rewrite(args.file_name, reinterpolate=args.reinterp)
        print(f"Rewrote file '{args.file_name}'.")


if __name__ == "__main__":
    main()
