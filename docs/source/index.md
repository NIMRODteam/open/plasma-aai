plasma-aai documentation
========================

Add your content using ``markdown`` syntax.


```{toctree}
:maxdepth: 2
:caption: Contents:
wiki/home.md
wiki/installation.md
wiki/Mathematics.md
wiki/Physics.md
wiki/Atomic-Data.md
wiki/Examples.md
wiki/Tests.md
```
