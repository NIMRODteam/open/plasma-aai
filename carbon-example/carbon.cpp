
#include <cstdio>
#include <cvode/cvode.h>
#include <iostream>
#include <memory>
#include <nvector/nvector_kokkos.hpp>
#include <sundials/sundials_core.hpp>
#include <sunlinsol/sunlinsol_spgmr.h>
#include <vector>

#include <plasma_config.hpp>
#include <rate_table.hpp>

using namespace plasma;

// Constants
#define ZERO SUN_RCONST(0.0)
#define ONE SUN_RCONST(1.0)
#define TWO SUN_RCONST(2.0)

// User-supplied functions called by CVODE
static int f(double t, N_Vector y, N_Vector ydot, void *user_data);

// User data structure available in user-supplied callback functions
struct UserData {
  int npts;
  int nspecies;
  int nbatches;
  UnmanagedViewType electron_dens;
  UnmanagedViewType electron_temp;
};

// Check for an unrecoverable (negative) return value from a SUNDIALS function
int check_flag(const int flag, const std::string funcname) {
  if (flag < 0) {
    std::cerr << "ERROR: " << funcname << " returned " << flag << std::endl;
    return 1;
  }
  return 0;
}

// Check if a function returned a NULL pointer
int check_ptr(const void *ptr, const std::string funcname) {
  if (ptr) {
    return 0;
  }
  std::cerr << "ERROR: " << funcname << " returned NULL" << std::endl;
  return 1;
}

/* -----------------------------------------------------------------------------
 * Main Program
 * ---------------------------------------------------------------------------*/

extern "C" int ode_eval(int npts, int nspecies, int nbatches, int solver_type,
                        double T0, double Tf, double rtol, double atol,
                        double *y0, double *electron_dens,
                        double *electron_temp) {
  using namespace sundials::experimental;

  Kokkos::parallel_for(
      "execspace-test", Kokkos::RangePolicy<ExecSpace>(0, 1),
      KOKKOS_LAMBDA(const SizeType i) {
#if (defined(USE_CUDA) && defined(__CUDA_ARCH__) && (__CUDA_ARCH__ > 0))
        printf("Kokkos is running on the gpu\n");
#else
	printf("Kokkos is running on the host\n");
#endif
  });

  sundials::Context sunctx;

  UserData udata;
  udata.npts = npts;
  udata.nspecies = nspecies;
  udata.nbatches = nbatches;

  init_rates();

  // Create the unmanaged Kokkos view for the state data
  UnmanagedViewType y_view(y0, npts * nspecies);
  UnmanagedViewType v_electron_dens(electron_dens, npts);
  udata.electron_dens = v_electron_dens;
  UnmanagedViewType v_electron_temp(electron_temp, npts);
  udata.electron_temp = v_electron_temp;

  // Create vector with the initial condition
  VecType y{y_view, sunctx};

  // Create CVODE and use Backward Differentiation Formula methods
  void *cvode_mem = CVodeCreate(CV_BDF, sunctx);
  if (check_ptr(cvode_mem, "CVodeCreate")) {
    return 1;
  }

  // Initialize the integrator and set the ODE right-hand side function
  int retval = CVodeInit(cvode_mem, f, T0, y);
  if (check_flag(retval, "CVodeInit")) {
    return 1;
  }

  // Attach the user data structure to CVODE
  retval = CVodeSetUserData(cvode_mem, &udata);
  if (check_flag(retval, "CVodeSetUserData")) {
    return 1;
  }

  // Specify the scalar relative and absolute tolerances
  retval = CVodeSStolerances(cvode_mem, rtol, atol);
  if (check_flag(retval, "CVodeSVtolerances")) {
    return 1;
  }

  // Create the linear solver
  SUNLinearSolverView LS{SUNLinSol_SPGMR(y, SUN_PREC_NONE, 0, sunctx)};

  // Attach the linear solver to CVODE
  retval = CVodeSetLinearSolver(cvode_mem, LS, nullptr);
  if (check_flag(retval, "CVodeSetLinearSolver")) {
    return 1;
  }

  // Advance in time
  retval = CVode(cvode_mem, Tf, y, &T0, CV_NORMAL);
  if (check_flag(retval, "CVode")) {
    return 1;
  }

  // Print integrator statistics for debugging and informational purposes
  CVodePrintAllStats(cvode_mem, stdout, SUN_OUTPUTFORMAT_TABLE);

  // Free the non RAII objects
  CVodeFree(&cvode_mem);

  return 0;
}

/* -----------------------------------------------------------------------------
 * User-supplied functions called by CVODE
 * ---------------------------------------------------------------------------*/

// Right hand side function dy/dt = f(t,y)
int f(double t, N_Vector y, N_Vector ydot, void *user_data) {
  auto udata = static_cast<UserData *>(user_data);
  auto y_data = sundials::kokkos::GetVec<VecType>(y)->View();
  auto ydot_data = sundials::kokkos::GetVec<VecType>(ydot)->View();

  const auto npts = udata->npts;
  const auto nspecies = udata->nspecies;
  const auto nbatches = udata->nbatches;

  const auto electron_temp = udata->electron_temp;
  const auto electron_dens = udata->electron_dens;

  Kokkos::Profiling::pushRegion("RHS");
  //std::cout << "At t=" << t << std::endl;

  Kokkos::parallel_for(
      "RHS", Kokkos::RangePolicy<ExecSpace>(0, npts),
      KOKKOS_LAMBDA(const SizeType i) {
    // update the electron density and initialize ydot
    electron_dens(i) = 0.0;
    for (int ispecies = 0; ispecies < nspecies; ispecies++) {
      auto i0 = i + ispecies * npts;
      ydot_data(i0) = 0.0;
      // TODO make a charge state array (on the GPU)
      // For now use ispecies as charge (it is)
      electron_dens(i) += y_data(i0) * ispecies;
    }

    // evaluate the rates
    //std::cout << "i=" << i << " te, ne:" << electron_temp(i) << " " << electron_dens(i) << std::endl;
    rate_table_t rates{std::log10(electron_temp(i)),
                       std::log10(electron_dens(i))};
    rate_interp(rates);

    // recombination and ionization
    for (int ispecies = 0; ispecies < nspecies - 1; ispecies++) {
      auto i0 = i + ispecies * npts;
      auto i1 = i + (ispecies + 1) * npts;
      //auto acd1 = plasma::math::powi<10>(rates.recomb[ispecies]);
      //auto scd0 = plasma::math::powi<10>(rates.ion[ispecies]);
      auto acd1 = std::pow(10,rates.recomb[ispecies]);
      auto scd0 = std::pow(10,rates.ion[ispecies]);
      ydot_data(i0) +=  acd1 * y_data(i1);
      ydot_data(i1) += -acd1 * y_data(i1);
      ydot_data(i0) += -scd0 * y_data(i0);
      ydot_data(i1) +=  scd0 * y_data(i0);
    }
    for (int ispecies = 0; ispecies < nspecies; ispecies++) {
      auto i0 = i + ispecies * npts;
      ydot_data(i0) *= electron_dens(i);
    }
  });
  Kokkos::Profiling::popRegion();

  return 0;
}
