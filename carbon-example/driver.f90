
module driver
  interface
    function ode_eval(npts, nspecies, nbatches, solver_type, t0, tf, &
                      rtol, atol, y0, ne, te) bind(C) result(status)
      use, intrinsic :: iso_c_binding
      implicit none
      integer(c_int), value :: npts, nspecies, nbatches, solver_type
      real(c_double), value :: t0, tf, rtol, atol
      type(c_ptr), value :: y0, ne, te
      integer(c_int) :: status
    end function
  end interface
end module driver

program main
  use, intrinsic :: iso_c_binding
  use driver
  use kokkos_fortran_interop
  implicit none

  integer(c_int), parameter :: nspecies = 7
  real(c_double), parameter :: charge_state(nspecies) = &
    [0., 1., 2., 3., 4., 5., 6.]
  integer(c_int), parameter :: ndens_pts = 512
  integer(c_int), parameter :: ntemp_pts = 512
  integer(c_int), parameter :: ntemp_batches = 1
  integer(c_int), parameter :: ndens_batches = 1
  integer(c_int) :: npts, npts_per_batch, nbatches, ndep
  integer(c_int) :: status, solver_type, ist, ien, ispecies, itemp, i
  real(c_double) :: t0, tf, rtol, atol, max_ion_error
  real(c_double) :: min_temp, max_temp, min_edens, max_edens
  real(c_double), allocatable, target :: y0(:), electron_temp(:), &
    electron_dens(:), sum_ion_dens_ref(:), sum_ion_dens_test(:)
  real(c_double) :: temp_ind(ntemp_pts),dens_ind(ndens_pts)

  do i=0,ntemp_pts-1
    temp_ind(i+1)=i/REAL(ntemp_pts-1)
  enddo
  do i=0,ndens_pts-1
    dens_ind(i+1)=i/REAL(ndens_pts-1)
  enddo

  ! We dont want to initialize Kokkos every time we call ode_eval.
  ! so we initialize it here in the driver.
  call kokkos_initialize_with_no_arguments()

  npts           = ndens_pts * ntemp_pts
  ndep           = npts * nspecies
  nbatches       = ndens_batches * ntemp_batches
  npts_per_batch = nbatches

  solver_type = 1

  allocate(y0(npts*nspecies)) ! carbon states C+0 to C+6
  allocate(electron_temp(npts))
  allocate(electron_dens(npts))
  allocate(sum_ion_dens_ref(npts))
  allocate(sum_ion_dens_test(npts))

  t0   = 0.0
  tf   = 1.e-10
  rtol = 1e-4
  atol = 1e-10

  ! Units: eV
  min_temp = 1.
  max_temp = 10.
  ! Units: m^-3
  min_edens = 1e18
  max_edens = 1e20

  ist=1
  do i=1,ntemp_pts
    ien=ist+ndens_pts-1
    electron_temp(ist:ien) = min_temp + temp_ind(i)*(max_temp-min_temp)
    ist=ist+ndens_pts
  enddo
  electron_dens = 0
  ist = 1
  do ispecies=0, nspecies-1
    ien=ist+ndens_pts-1
    if (ispecies > 0) then
      do i=0, ndens_pts-1
        y0(ist+i) = (min_edens + dens_ind(i+1)*(max_edens-min_edens)) &
                    /((nspecies-1)*charge_state(ispecies+1))
      enddo
    else
      do i=0, ndens_pts-1
        y0(ist+i) = (min_edens + dens_ind(i+1)*(max_edens-min_edens)) &
                    /(nspecies-1)
      enddo
    endif
    do itemp = 1, ntemp_pts-1
      y0(ist+itemp*ndens_pts:ien+itemp*ndens_pts) = y0(ist:ien)
    enddo
    ien=ist+npts-1
    electron_dens = electron_dens + y0(ist:ien)*charge_state(ispecies+1)
    sum_ion_dens_ref = sum_ion_dens_ref + y0(ist:ien)
    ist=ist+npts
  enddo

  !$acc enter data copyin(y0(:), electron_temp(:), electron_dens(:))

  ! TODO write h5 file w/ initial state
  write(*,'(a)') 'Plasma initial densities (e, C+0 to C+6) at 4 points:'
  write(*,'(8es10.3)') electron_dens(1),y0(1:ndep:npts)
  write(*,'(8es10.3)') electron_dens(ndens_pts/3),y0(ndens_pts/3:ndep:npts)
  write(*,'(8es10.3)') electron_dens(2*ndens_pts/3),y0(2*ndens_pts/3:ndep:npts)
  write(*,'(8es10.3)') electron_dens(ndens_pts),y0(ndens_pts:ndep:npts)

  !$acc wait

  ! TODO normalize variables

  !$acc host_data use_device(y0,electron_temp,electron_dens)
  status = ode_eval(npts, nspecies, nbatches, solver_type, t0, tf, rtol, atol, &
                    c_loc(y0), c_loc(electron_dens), c_loc(electron_temp))
  if (status /= 0) then
    write(*, *) 'not ok: ERROR in ode_eval function'
  end if
  !$acc end host_data

  ! TODO unnormalize variables

  !$acc update self(y0,electron_temp,electron_dens)
  !$acc wait
  ! TODO write h5 file w/ final state

  electron_dens = 0
  ist = 1
  do ispecies=0, nspecies-1
    ien=ist+npts-1
    electron_dens = electron_dens + y0(ist:ien)*charge_state(ispecies+1)
    sum_ion_dens_test = sum_ion_dens_test + y0(ist:ien)
    ist=ist+npts
  enddo

  write(*,'(a)') 'Plasma final densities (e, C+0 to C+6) at 4 points:'
  write(*,'(8es10.3)') electron_dens(1),y0(1:ndep:npts)
  write(*,'(8es10.3)') electron_dens(ndens_pts/3),y0(ndens_pts/3:ndep:npts)
  write(*,'(8es10.3)') electron_dens(2*ndens_pts/3),y0(2*ndens_pts/3:ndep:npts)
  write(*,'(8es10.3)') electron_dens(ndens_pts),y0(ndens_pts:ndep:npts)

  max_ion_error= &
    maxval(abs((sum_ion_dens_test-sum_ion_dens_ref)/sum_ion_dens_ref))
  write(*,'(a,es10.3)') 'Max error in total ion number density conservation: ', &
    max_ion_error
  if (max_ion_error>1.e-10) then
    write(*,'(a)') 'not ok: Ion number density not conserved.'
    if (status==0) status=-1
  endif

  !$acc exit data delete(y0,electron_temp,electron_dens)
  deallocate(y0,electron_temp,electron_dens)
  deallocate(sum_ion_dens_test,sum_ion_dens_ref)

  call kokkos_finalize()

  stop status

end program main
