
module kokkos_fortran_interop
  use iso_c_binding

  interface
    subroutine kokkos_initialize_with_no_arguments() bind(C)
    end subroutine

    subroutine kokkos_finalize() bind(C)
    end subroutine
  end interface

end module kokkos_fortran_interop
