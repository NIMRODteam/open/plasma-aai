#ifndef PLASMAAAI_MATH_H_
#define PLASMAAAI_MATH_H_

#include <cmath>
#include <cstdlib>
#include <type_traits>
#include <utility>

namespace plasma {
namespace math {

//! Return pow(x, Power), where Power is an integer known at compile time
template <int Power, typename T,
          typename = std::enable_if_t<!std::is_integral<T>() || Power >= 0>>
KOKKOS_INLINE_FUNCTION constexpr T powi(T x) noexcept {
  if constexpr (Power < 0) {
    return T(1) / powi<-Power>(x);
  } else if constexpr (Power == 0) {
    // note: 0^0 is implementation-defined, but most compilers return 1
    return T(1);
  } else if constexpr (Power == 1) {
    return x;
  } else if constexpr (Power == 2) {
    return x * x;
  } else if constexpr (Power % 2 == 0) {
    return powi<2>(powi<Power / 2>(x));
  } else {
    return x * powi<Power - 1>(x);
  }
}

} // namespace math
} // namespace plasma

#endif
