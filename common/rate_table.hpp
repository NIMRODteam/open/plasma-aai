/*
The following license applies to all code that is not explicitly
marked as covered by a different license, as indicated by a
directory-level license.txt in that sub-directory.

------------------------------------------------------------------------
StarKiller Microphysics License
------------------------------------------------------------------------

Copyright 2019 StarKiller Microphysics Development Team

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef RATE_TABLE_H
#define RATE_TABLE_H

#include <Kokkos_Core.hpp>

#include <H5Cpp.h>
#include <array>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>

#include <memory>
#include <rate_table_data.hpp>
#include <rate_table_size.hpp>
#include <rate_table_type.hpp>
#include <plasma_math.hpp>

// change to 1 for linear instead of cubic interpolation
constexpr int rate_table_interp_linear = 0;

///
/// given a rho, T index, and species index return the 1-d index into the
/// table arrays
///
PLASMA_GPU_HOST_DEVICE inline int rate_idx(const int ir, const int it,
                                           const int ispec) {
  return (ispec * rate_table_size::ntemp * rate_table_size::nden) +
         (ir * rate_table_size::ntemp) + it;
}

template <typename T>
inline void print_rate_table(const int ispec, const T &data) {
  for (int ir = 0; ir < rate_table_size::nden; ++ir) {
    for (int it = 0; it < rate_table_size::ntemp; ++it) {
      plasma::Print() << std::scientific << std::setprecision(8)
                << data[rate_idx(ir, it, ispec)] << ", ";
    }
    plasma::Print() << "\n";
  }
}

template <typename T, size_t N>
inline void read_h5_data(std::array<T, N> &data, H5::H5File &file,
                         std::string datasetname) {

  std::array<T, N> hdf5_ordered_data{};

  H5::DataSet dataset = file.openDataSet(datasetname);
  H5::DataSpace dataspace = dataset.getSpace();
  int rank = dataspace.getSimpleExtentNdims();
  hsize_t dims_out[rank];
  int ndims = dataspace.getSimpleExtentDims(dims_out, NULL);
  int total_size = 1;
  for (int ii = 0; ii < ndims; ii++) {
    total_size *= int(dims_out[ii]);
  }
  dataset.read(hdf5_ordered_data.data(), H5::PredType::NATIVE_DOUBLE, dataspace,
               dataspace);


  // reorder such that nspec is slowest index, then rho, then T
  for (int i = 0; i < rate_table_size::nden; ++i) {
    for (int j = 0; j < rate_table_size::ntemp; ++j) {
      for (int k = 0; k < rate_table_size::nspec; ++k) {
        int old_idx = (i * rate_table_size::ntemp * rate_table_size::nspec) +
                      (j * rate_table_size::nspec) + k;
        data[rate_idx(i, j, k)] = hdf5_ordered_data[old_idx];
      }
    }
  }
}

inline void init_rates() {

  std::string file_name("./" + rate_table_size::table_name);
  if (const char* env_path_c = std::getenv("PLASMA_ATOMIC_DATA_PATH")) {
    std::string env_path = env_path_c;
    file_name = env_path + "/" + rate_table_size::element
                         + "/" + rate_table_size::table_name;
  }
  plasma::Print() << "Reading the rate table " + file_name + " ..." << std::endl;

  H5::H5File file(file_name, H5F_ACC_RDONLY);

  // read_h5_data(Ne, file, "/acd/logNe");
  // read_h5_data(T, file, "/acd/logT");
  read_h5_data(rate_table::recombtab, file, "/acd/logdata");
  read_h5_data(rate_table::iontab, file, "/scd/logdata");

  // print_rate_table(0, rate_table::recombtab);

  plasma::Print() << "done." << std::endl;
}

PLASMA_GPU_HOST_DEVICE inline sunrealtype rate_table_logT(const int it) {
  return rate_table_size::logT_min +
         static_cast<sunrealtype>(it) * rate_table_size::dlogT;
}

PLASMA_GPU_HOST_DEVICE inline sunrealtype rate_table_logrho(const int ir) {
  return rate_table_size::logrho_min +
         static_cast<sunrealtype>(ir) * rate_table_size::dlogrho;
}

// return the index in the table such that logrho[irho] < input density
PLASMA_GPU_HOST_DEVICE inline int
rate_get_logrho_index(const sunrealtype logrho) {

  int ir0 = static_cast<int>(
      (logrho - rate_table_size::logrho_min) / rate_table_size::dlogrho - 1.e-6);
  return ir0;
}

// return the index in the table such that logT[it] < input temperature
PLASMA_GPU_HOST_DEVICE inline int rate_get_logT_index(const sunrealtype logT) {

  int it0 = static_cast<int>(
      (logT - rate_table_size::logT_min) / rate_table_size::dlogT - 1.e-6);
  return it0;
}

///
/// given 4 points (xs, fs), with spacing dx, return the interpolated
/// value of f at point x by fitting a cubic to the points
///
PLASMA_GPU_HOST_DEVICE inline sunrealtype cubic(const sunrealtype *xs,
                                                const sunrealtype *fs,
                                                const sunrealtype dx,
                                                const sunrealtype x) {

  // fit a cubic of the form
  // f(x) = a (x - x_i)**3 + b (x - x_i)**2 + c (x - x_i) + d
  // to the data (xs, fs)
  // we take x_i to be x[1]

  sunrealtype a =
      (3 * fs[1] - 3 * fs[2] + fs[3] - fs[0]) / (6 * plasma::math::powi<3>(dx));
  sunrealtype b = (-2 * fs[1] + fs[2] + fs[0]) / (2 * dx * dx);
  sunrealtype c = (-3 * fs[1] + 6 * fs[2] - fs[3] - 2 * fs[0]) / (6 * dx);
  sunrealtype d = fs[1];

  return a * plasma::math::powi<3>(x - xs[1]) +
         b * plasma::math::powi<2>(x - xs[1]) + c * (x - xs[1]) + d;
}

///
/// given 4 points (xs, fs), with spacing dx between the xs, return
/// the derivative of f at point x by fitting a cubic to the
/// points and differentiating the interpolant
///
PLASMA_GPU_HOST_DEVICE inline sunrealtype cubic_deriv(const sunrealtype *xs,
                                                      const sunrealtype *fs,
                                                      const sunrealtype dx,
                                                      const sunrealtype x) {

  // fit a cubic of the form
  // f(x) = a (x - x_i)**3 + b (x - x_i)**2 + c (x - x_i) + d
  // to the data (xs, fs)
  // we take x_i to be x[1]
  // then return dfdx = 3 a (x - x_i)**2 + 2 b (x - x_i) + c

  sunrealtype a =
      (3 * fs[1] - 3 * fs[2] + fs[3] - fs[0]) / (6 * plasma::math::powi<3>(dx));
  sunrealtype b = (-2 * fs[1] + fs[2] + fs[0]) / (2 * dx * dx);
  sunrealtype c = (-3 * fs[1] + 6 * fs[2] - fs[3] - 2 * fs[0]) / (6 * dx);
  // sunrealtype d = fs[1];

  return 3.0 * a * plasma::math::powi<2>(x - xs[1]) + 2.0 * b * (x - xs[1]) + c;
}

template <typename T, typename U>
PLASMA_GPU_HOST_DEVICE inline void
bilinear(const int ir1, const int it1, const sunrealtype rho,
         const sunrealtype temp, const T &data, U &val) {

  for (int ispec = 0; ispec < rate_table_size::nspec; ++ispec) {
    // find the four interpolation points in the 1D arrays
    int it1r1 = rate_idx(ir1, it1, ispec);
    int it1r2 = rate_idx(ir1 + 1, it1, ispec);
    int it2r1 = rate_idx(ir1, it1 + 1, ispec);
    int it2r2 = rate_idx(ir1 + 1, it1 + 1, ispec);

    sunrealtype t0 = rate_table_logT(it1);
    sunrealtype r0 = rate_table_logrho(ir1);

    sunrealtype td = (temp - t0) / rate_table_size::dlogT;
    sunrealtype rd = (rho - r0) / rate_table_size::dlogrho;

    sunrealtype omtd = 1.0 - td;
    sunrealtype omrd = 1.0 - rd;

    val[ispec] = data[it1r1] * omtd * omrd + data[it1r2] * omtd * rd +
                 data[it2r1] * td * omrd + data[it2r2] * td * rd;
  }
}

template <typename T, typename U>
PLASMA_GPU_HOST_DEVICE inline void
bicubic(const int ir0, const int it0, const sunrealtype rho,
        const sunrealtype temp, const T &data, U &val) {

  for (int ispec = 0; ispec < rate_table_size::nspec; ++ispec) {
    const sunrealtype Ts[] = {rate_table_logT(it0), rate_table_logT(it0 + 1),
                              rate_table_logT(it0 + 2), rate_table_logT(it0 + 3)};

    const sunrealtype rhos[] = {
        rate_table_logrho(ir0), rate_table_logrho(ir0 + 1),
        rate_table_logrho(ir0 + 2), rate_table_logrho(ir0 + 3)};

    // first do the 4 T interpolations
    sunrealtype d1[4];
    for (int ii = 0; ii < 4; ++ii) {
      const sunrealtype _d[] = {data[rate_idx(ir0 + ii, it0, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 1, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 2, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 3, ispec)]};
      d1[ii] = cubic(Ts, _d, rate_table_size::dlogT, temp);
    }

    // finally do the remaining interpolation over rho
    val[ispec] = cubic(rhos, d1, rate_table_size::dlogrho, rho);
  }
}

///
/// take the temperature derivative of a table quantity by differentiating
/// the cubic interpolant
///
template <typename T, typename U>
PLASMA_GPU_HOST_DEVICE inline void
bicubic_dT(const int ir0, const int it0, const sunrealtype rho,
           const sunrealtype temp, const T &data, U &val) {

  for (int ispec = 0; ispec < rate_table_size::nspec; ++ispec) {
    const sunrealtype Ts[] = {rate_table_logT(it0), rate_table_logT(it0 + 1),
                              rate_table_logT(it0 + 2), rate_table_logT(it0 + 3)};

    const sunrealtype rhos[] = {
        rate_table_logrho(ir0), rate_table_logrho(ir0 + 1),
        rate_table_logrho(ir0 + 2), rate_table_logrho(ir0 + 3)};

    // do the 4 rho interpolations (one in each T plane)
    sunrealtype d2[4];
    for (int jj = 0; jj < 4; ++jj) {
      const sunrealtype _d[] = {data[rate_idx(ir0, it0 + jj, ispec)],
                                data[rate_idx(ir0 + 1, it0 + jj, ispec)],
                                data[rate_idx(ir0 + 2, it0 + jj, ispec)],
                                data[rate_idx(ir0 + 3, it0 + jj, ispec)]};
      d2[jj] = cubic(rhos, _d, rate_table_size::dlogrho, rho);
    }

    // finally do the remaining interpolation over T, but return
    // the derivative of the interpolant
    val[ispec] = cubic_deriv(Ts, d2, rate_table_size::dlogT, temp);
  }
}

///
/// take the density derivative of a table quantity by differentiating
/// the cubic interpolant
///
template <typename T, typename U>
PLASMA_GPU_HOST_DEVICE inline void
bicubic_drho(const int ir0, const int it0, const sunrealtype rho,
             const sunrealtype temp, const T &data, U &val) {

  for (int ispec = 0; ispec < rate_table_size::nspec; ++ispec) {
    const sunrealtype Ts[] = {rate_table_logT(it0), rate_table_logT(it0 + 1),
                              rate_table_logT(it0 + 2), rate_table_logT(it0 + 3)};

    const sunrealtype rhos[] = {
        rate_table_logrho(ir0), rate_table_logrho(ir0 + 1),
        rate_table_logrho(ir0 + 2), rate_table_logrho(ir0 + 3)};

    // do the 4 T interpolations (one in each rho plane)
    sunrealtype d2[4];
    for (int ii = 0; ii < 4; ++ii) {
      const sunrealtype _d[] = {data[rate_idx(ir0 + ii, it0, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 1, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 2, ispec)],
                                data[rate_idx(ir0 + ii, it0 + 3, ispec)]};
      d2[ii] = cubic(Ts, _d, rate_table_size::dlogT, temp);
    }

    // do the remaining interpolation over rho, but return
    // the derivative of the interpolant
    val[ispec] = cubic_deriv(rhos, d2, rate_table_size::dlogrho, rho);
  }
}

PLASMA_GPU_HOST_DEVICE inline void rate_interp(rate_table_t &rate_state) {

  using namespace rate_table;

  sunrealtype rholog = std::log10(rate_state.rho);
  {
    sunrealtype rmin = rate_table_size::logrho_min;
    sunrealtype rmax = rate_table_size::logrho_max;

    rholog = std::clamp(rholog, rmin, rmax);
  }

  sunrealtype tlog = std::log10(rate_state.T);
  {
    sunrealtype tmin = rate_table_size::logT_min;
    sunrealtype tmax = rate_table_size::logT_max;

    tlog = std::clamp(tlog, tmin, tmax);
  }

  if (rate_table_interp_linear) {

    int ir1 = rate_get_logrho_index(rholog);
    int it1 = rate_get_logT_index(tlog);

    if (!rate_state.recomb.empty()) {
      bilinear(ir1, it1, rholog, tlog, recombtab, rate_state.recomb);
    }
    if (!rate_state.ion.empty()) {
      bilinear(ir1, it1, rholog, tlog, iontab, rate_state.ion);
    }
    if (!rate_state.cx.empty()) {
      bilinear(ir1, it1, rholog, tlog, cxtab, rate_state.cx);
    }
    if (!rate_state.exlinerad.empty()) {
      bilinear(ir1, it1, rholog, tlog, exlineradtab, rate_state.exlinerad);
    }
    if (!rate_state.recombrad.empty()) {
      bilinear(ir1, it1, rholog, tlog, recombradtab, rate_state.recombrad);
    }
    if (!rate_state.cxlinerad.empty()) {
      bilinear(ir1, it1, rholog, tlog, cxlineradtab, rate_state.cxlinerad);
    }

  } else {

    // for a cubic interpolant, we need 4 points that span the data value
    // for temperature, these will be it0, it0+1, it0+2, it0+3
    // with the idea that the temperature we want is between it0+1 and it0+2
    // so we offset one to the left and also ensure that we don't go off the
    // table

    int ir0 = rate_get_logrho_index(rholog) - 1;
    ir0 = std::clamp(ir0, 1, rate_table_size::nden - 3);

    int it0 = rate_get_logT_index(tlog) - 1;
    it0 = std::clamp(it0, 1, rate_table_size::ntemp - 3);

    if (!rate_state.recomb.empty()) {
      bicubic(ir0, it0, rholog, tlog, recombtab, rate_state.recomb);
    }
    if (!rate_state.ion.empty()) {
      bicubic(ir0, it0, rholog, tlog, iontab, rate_state.ion);
    }
    if (!rate_state.cx.empty()) {
      bicubic(ir0, it0, rholog, tlog, cxtab, rate_state.cx);
    }
    if (!rate_state.exlinerad.empty()) {
      bicubic(ir0, it0, rholog, tlog, exlineradtab, rate_state.exlinerad);
    }
    if (!rate_state.recombrad.empty()) {
      bicubic(ir0, it0, rholog, tlog, recombradtab, rate_state.recombrad);
    }
    if (!rate_state.cxlinerad.empty()) {
      bicubic(ir0, it0, rholog, tlog, cxlineradtab, rate_state.cxlinerad);
    }
  }
}

///
/// compute the temperature derivative of the table quantity data
/// at the point T, rho by using cubic interpolation
///
template <typename T>
PLASMA_GPU_HOST_DEVICE inline sunrealtype
rate_interp_dT(const sunrealtype temp, const sunrealtype rho, const T &data) {
  sunrealtype rholog = std::log10(rho);
  {
    sunrealtype rmin = rate_table_size::logrho_min;
    sunrealtype rmax = rate_table_size::logrho_max;

    rholog = std::clamp(rholog, rmin, rmax);
  }

  sunrealtype tlog = std::log10(temp);
  {
    sunrealtype tmin = rate_table_size::logT_min;
    sunrealtype tmax = rate_table_size::logT_max;

    tlog = std::clamp(tlog, tmin, tmax);
  }

  int ir0 = rate_get_logrho_index(rholog) - 1;
  ir0 = std::clamp(ir0, 1, rate_table_size::nden - 3);

  int it0 = rate_get_logT_index(tlog) - 1;
  it0 = std::clamp(it0, 1, rate_table_size::ntemp - 3);

  // note: this is returning the derivative wrt log10(T), so we need to
  // convert to d/dT

  sunrealtype ddatadT =
      bicubic_dT(ir0, it0, rholog, tlog, data) / (std::log(10.0) * temp);

  return ddatadT;
}

///
/// compute the density derivative of the table quantity data
/// at the point T, rho by using cubic interpolation
///
template <typename T>
PLASMA_GPU_HOST_DEVICE inline sunrealtype
rate_interp_drho(const sunrealtype temp, const sunrealtype rho, const T &data) {
  sunrealtype rholog = std::log10(rho);
  {
    sunrealtype rmin = rate_table_size::logrho_min;
    sunrealtype rmax = rate_table_size::logrho_max;

    rholog = std::clamp(rholog, rmin, rmax);
  }

  sunrealtype tlog = std::log10(temp);
  {
    sunrealtype tmin = rate_table_size::logT_min;
    sunrealtype tmax = rate_table_size::logT_max;

    tlog = std::clamp(tlog, tmin, tmax);
  }

  int ir0 = rate_get_logrho_index(rholog) - 1;
  ir0 = std::clamp(ir0, 1, rate_table_size::nden - 3);

  int it0 = rate_get_logT_index(tlog) - 1;
  it0 = std::clamp(it0, 1, rate_table_size::ntemp - 3);

  // note: this is returning the derivative wrt log10(rho), so we need to
  // convert to d/drho

  sunrealtype ddatadrho =
      tricubic_drho(ir0, it0, rholog, tlog, data) / (std::log(10.0) * rho);

  return ddatadrho;
}

#endif
