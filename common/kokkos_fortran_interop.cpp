#include <Kokkos_Core.hpp>

extern "C"
{

void kokkos_initialize_with_no_arguments()
{
  Kokkos::initialize();
}

void kokkos_finalize()
{
  Kokkos::finalize();
}

}
