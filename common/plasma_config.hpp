#ifndef PLASMAAAI_GPU_H_
#define PLASMAAAI_GPU_H_

#include <nvector/nvector_kokkos.hpp>

#ifdef USE_CUDA
#define PLASMA_GPU_HOST __host__
#define PLASMA_GPU_DEVICE __device__
#define PLASMA_GPU_MANAGED __device__ __managed__
#define PLASMA_GPU_HOST_DEVICE __host__ __device__
#else
#define PLASMA_GPU_HOST
#define PLASMA_GPU_DEVICE
#define PLASMA_GPU_MANAGED
#define PLASMA_GPU_HOST_DEVICE
#endif

namespace plasma {

// Execution space
#ifdef USE_CUDA
using ExecSpace = Kokkos::DefaultExecutionSpace;
using HostExecSpace = Kokkos::DefaultHostExecutionSpace;;
#else
using ExecSpace = Kokkos::DefaultHostExecutionSpace;
using HostExecSpace = Kokkos::DefaultHostExecutionSpace;
#endif

using VecType = sundials::kokkos::Vector<ExecSpace>;
using SizeType = VecType::size_type;
using ViewType = sundials::kokkos::Vector<ExecSpace>::view_type;
using UnmanagedViewType =
    Kokkos::View<double *, ExecSpace, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
using UnmanagedHostViewType =
    Kokkos::View<double *, HostExecSpace, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;

// TODO(CJB): add our own stream (or Print class) which can be configured
// For now, we just alias to std::cout.
std::ostream& Print() {
    return std::cout;
}

}

#endif // PLASMAAAI_GPU_H_

