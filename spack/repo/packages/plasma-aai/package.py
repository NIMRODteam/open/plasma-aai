# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PlasmaAai(CMakePackage, CudaPackage):
    """Integrates ordinary differential equations associated with local plasma
    atomic interaction
    """

    homepage = "https://gitlab.com/NIMRODteam/open/plasma-aai"
    git = "https://gitlab.com/NIMRODteam/open/plasma-aai.git"

    maintainers("balos1", "gardner48", "jacobrking")

    version("main", branch="main")
    license("BSD-3-Clause")

    variant("fetch_adas", default=False, description="Whether to fetch ADAS data")
    variant("plasma_atomic_data_path", default="auto", description="Where to store atomic data")
    variant("enable_tests", default=False, description="Whether to enable tests or not")

    depends_on("cmake", type="build")
    depends_on("hdf5+cxx+fortran~mpi")
    depends_on("sundials+kokkos+f2003~mpi")

    depends_on("sundials+cuda", when="+cuda")
    depends_on("kokkos+wrapper+cuda", when="+cuda")

    def cmake_args(self):

        args = [
            self.define_from_variant("ENABLE_CUDA", "cuda"),
            self.define_from_variant("FETCH_ADAS", "fetch_adas"),
            self.define_from_variant("ENABLE_TESTS", "enable_tests")
        ]

        if "+cuda" in self.spec:
            args.append(self.define("CMAKE_CUDA_ARCHITECTURES", \
                                    self.spec.variants["cuda_arch"].value))

        if "plasma_atomic_data_path=auto" not in self.spec:
            args.append(self.define_from_variant("PLASMA_ATOMIC_DATA_PATH", \
                                                 "plasma_atomic_data_path"))

        return args

    @run_after("build")
    @on_package_attributes(run_tests=True)
    def check_test_install(self):
        """Run the plasma-aai unit tests in the build directory."""
        with working_dir(self.build_directory):
            make("test")
