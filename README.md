# Plasma accelerated atomic integration (plasma-aai)

plasma-aai integrates ordinary differential equations (ODEs) associated with local plasma
atomic interaction from processes such as ionization, recombinations, charge-exchange, radiation,
collisional friction and collisional energy-exchange. The [SUNDIALS](https://computing.llnl.gov/projects/sundials)
library drives the intergration routines and the [Kokkos](https://github.com/kokkos/kokkos) programming model
is used for performance portability across CPU and GPU platforms.

plasma-aai is being developed as part of the [CETOP](https://sites.google.com/pppl.gov/cetop-elmscidac/home)
SciDAC-5 partnership project funded by the U.S. Department of Energy.

Plans for future development include:
- Application of multirate infinitesimal methods to reduce the computational cost
- Code-generation tools to generate flexible integrands that accommodate the wide
  variety of potential combinations of different species
- More parallelism with asynchronous kernel execute (e.g. CUDA streams)
- Testing: use python to compare final equilibrated state with
  ionization/recombination equilibrium without radiation.

## Quick Start

Full documentation can be found at
[nimrodteam.gitlab.io/open/plasma-aai/](https://nimrodteam.gitlab.io/open/plasma-aai/).

Presently a carbon example with CVODE is available that may be compiled to run
on the GPU. Rates may be generated with the with the `scripts/make_adata_file.py`
script. This script can either create a synthetic test data file, or pull rates
from ADAS using the [Aurora fusion](https://aurora-fusion.readthedocs.io/en/latest/) python package.
The most recent version of the Aurora package may be installed with pip via the command:

```shell
pip install https://github.com/fsciortino/Aurora@master
```

A simple, flat-array interface is used where the data may be resident on the GPU.


### Running The Carbon Example

After building, to run the Carbon test case within the spack environment:

```shell
$ driver
```

The example expects the rate data file to be present in the current directory or
as specified from a element-specific subdirectory of the environment variable
`PLASMA_ATOMIC_DATA_PATH`. Atomic data will be generated in this directory when
configured with `-DFETCH_ADAS=TRUE`.
