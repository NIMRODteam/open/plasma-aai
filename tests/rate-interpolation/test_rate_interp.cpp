
#include <gtest/gtest.h>
#include <rate_table.hpp>
#include <rate_table_type.hpp>
#include <rate_table_size.hpp>

using namespace rate_table;

class RateTableTest : public ::testing::Test {
protected:
  void SetUp() override {
    // Code here will be called immediately after the constructor (right before each test).
    init_rates();
  }

  void TearDown() override {
    // Code here will be called immediately after each test (right before the destructor).
  }

};

TEST_F(RateTableTest, RateTableLogTIndexAtMinMax) {
  EXPECT_EQ(rate_get_logT_index(rate_table_size::logT_min), 0);
  EXPECT_EQ(rate_get_logT_index(rate_table_size::logT_max), rate_table_size::ntemp-2);
}

TEST_F(RateTableTest, RateTableLogTIndexAtPoint) {
  for (int i = 1; i < rate_table_size::ntemp-1; i++) {
    sunrealtype T = rate_table_size::logT_min + i*rate_table_size::dlogT;
    EXPECT_EQ(rate_get_logT_index(T), i-1);
  }
}

TEST_F(RateTableTest, RateTableLogTIndexOffPoint) {
  // Perturb T such that it is not exactly a T value we know, but should still give the same index as T
  for (int i = 1; i < rate_table_size::ntemp-1; i++) {
    sunrealtype T = rate_table_size::logT_min + i*rate_table_size::dlogT;
    sunrealtype Tperturbed = T - rate_table_size::dlogT/2.0;
    EXPECT_EQ(rate_get_logT_index(Tperturbed), rate_get_logT_index(T));
  }

  // Perturb T such that it should give the next index as compared to the index for T
  for (int i = 1; i < rate_table_size::ntemp-1; i++) {
    sunrealtype T = rate_table_size::logT_min + i*rate_table_size::dlogT;
    sunrealtype Tperturbed = T + rate_table_size::dlogT/2.0;
    EXPECT_EQ(rate_get_logT_index(Tperturbed), rate_get_logT_index(T)+1);
  }
}

TEST_F(RateTableTest, RateTableLogRhoIndexAtMinMax) {
  EXPECT_EQ(rate_get_logrho_index(rate_table_size::logrho_min), 0);
  EXPECT_EQ(rate_get_logrho_index(rate_table_size::logrho_max), rate_table_size::nden-2);
}

TEST_F(RateTableTest, RateTableLogRhoIndexAtPoint) {
  for (int i = 1; i < rate_table_size::nden-1; i++) {
    sunrealtype rho = rate_table_size::logrho_min + i*rate_table_size::dlogrho;
    EXPECT_EQ(rate_get_logrho_index(rho), i-1);
  }
}

TEST_F(RateTableTest, RateTableLogRhoIndexOffPoint) {
  // Perturb rho such that it is not exactly a rho value we know, but should still give the same index as rho
  for (int i = 1; i < rate_table_size::nden-1; i++) {
    sunrealtype rho = rate_table_size::logrho_min + i*rate_table_size::dlogrho;
    sunrealtype rho_perturbed = rho - rate_table_size::dlogrho/2.0;
    EXPECT_EQ(rate_get_logrho_index(rho_perturbed), rate_get_logrho_index(rho));
  }

  // Perturb rho such that it should give the next index as compared to the index for rho
  for (int i = 1; i < rate_table_size::nden-1; i++) {
    sunrealtype rho = rate_table_size::logrho_min + i*rate_table_size::dlogrho;
    sunrealtype rho_perturbed = rho + rate_table_size::dlogrho/2.0;
    EXPECT_EQ(rate_get_logrho_index(rho_perturbed), rate_get_logrho_index(rho)+1);
  }
}


TEST_F(RateTableTest, RateInterpTestAtPoint) {
  // Test interpolation by selecting a point that is available in the lookup table.
  constexpr sunrealtype eps = sunrealtype{100.0}*std::numeric_limits<sunrealtype>::epsilon();

  int midpointT = rate_table_size::ntemp / 2;
  int midpointElec = rate_table_size::nden / 2;
  sunrealtype testT = rate_table_size::logT_min + midpointT*rate_table_size::dlogT;
  sunrealtype testElec = rate_table_size::logrho_min + midpointElec*rate_table_size::dlogrho;

  // rate_interp will take the log10 of the T and rho
  rate_table_t test_point{std::pow(10, testT), std::pow(10, testElec)};
  rate_interp(test_point);

  for (int i = 0; i < rate_table_size::nspec; i++) {
    sunrealtype expected_recomb = recombtab[rate_idx(midpointElec, midpointT, i)];
    sunrealtype expected_ion = iontab[rate_idx(midpointElec, midpointT, i)];
    if (!recombtab.empty()) {
      EXPECT_NEAR(test_point.recomb[i], expected_recomb, eps);
    }
    if (!iontab.empty()) {
      EXPECT_NEAR(test_point.ion[i], expected_ion, eps);
    }
    if (!cxtab.empty()) {
      sunrealtype expected_cx = cxtab[rate_idx(midpointElec, midpointT, i)];
      EXPECT_NEAR(test_point.cx[i], expected_cx, eps);
    }
    if (!exlineradtab.empty()) {
      sunrealtype expected_exlinerad = exlineradtab[rate_idx(midpointElec, midpointT, i)];
      EXPECT_NEAR(test_point.exlinerad[i], expected_exlinerad, eps);
    }
    if (!recombradtab.empty()) {
      sunrealtype expected_recombrad = recombradtab[rate_idx(midpointElec, midpointT, i)];
      EXPECT_NEAR(test_point.recombrad[i], expected_recombrad, eps);
    }
    if (!cxlineradtab.empty()) {
      sunrealtype expected_cxlinerad = cxlineradtab[rate_idx(midpointElec, midpointT, i)];
      EXPECT_NEAR(test_point.cxlinerad[i], expected_cxlinerad, eps);
    }
  }
}
