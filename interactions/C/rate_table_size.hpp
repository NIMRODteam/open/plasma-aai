/*
The following license applies to all code that is not explicitly
marked as covered by a different license, as indicated by a
directory-level license.txt in that sub-directory.

------------------------------------------------------------------------
StarKiller Microphysics License
------------------------------------------------------------------------

Copyright 2019 StarKiller Microphysics Development Team

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef RATE_TABLE_SIZE_H
#define RATE_TABLE_SIZE_H

#include <string>
#include <sundials/sundials_types.h>

namespace rate_table_size {

const std::string table_name{"atomic_data_C.h5"};
const std::string element{"C"};

constexpr int ntemp = 30;
constexpr int nden = 24;
constexpr int nspec = 6;

/*
  Cross Validation Scores from Re-interpolation of ADAS rates onto uniform grid:
  acd Maximum Re-interpolation nRMSE = -1.469996e-03
  ccd Maximum Re-interpolation nRMSE = -1.083788e-03
  plt Maximum Re-interpolation nRMSE = -3.429082e-03
  prb Maximum Re-interpolation nRMSE = -6.107532e-04
  prc Maximum Re-interpolation nRMSE = -1.740497e-16
  scd Maximum Re-interpolation nRMSE = -1.816610e-02
 */

constexpr sunrealtype logT_min = -6.988e-1;
constexpr sunrealtype logT_max = 4.176e+0;
constexpr sunrealtype dlogT = (logT_max-logT_min)/(ntemp-1);

constexpr sunrealtype logrho_min = 7.699e+0;
constexpr sunrealtype logrho_max = 1.530e+1;
constexpr sunrealtype dlogrho = (logrho_max-logrho_min)/(nden-1);

} // namespace rate_table_size
#endif
